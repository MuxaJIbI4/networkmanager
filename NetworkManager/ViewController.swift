//
//  ViewController.swift
//  NetworkManager
//
//  Created by Кустов Михаил on 12/9/19.
//  Copyright © 2019 Кустов Михаил. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var dataFetcherService = DataFetcherService()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        dataFetcherService.fetchStars { (lists) in
            print(lists?.stars?.star.first?.name)
        }
        
//        dataFetcherService.fetchFreeGames { (freeGames) in
//            print(freeGames?.feed.results.first?.name ?? "")
//        }
//
//        dataFetcherService.fetchNewGames { (newGames) in
//            print(newGames?.feed.results.first?.name ?? "")
//        }
//
//        dataFetcherService.fetchLocalCountry { (localCountries) in
//            print(localCountries?.last?.Name ?? "")
//        }
    }


}

