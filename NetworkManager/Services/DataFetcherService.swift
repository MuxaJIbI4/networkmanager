//
//  DataFetcherService.swift
//  NetworkManager
//
//  Created by Кустов Михаил on 12/9/19.
//  Copyright © 2019 Кустов Михаил. All rights reserved.
//

import Foundation

public enum Types: String {
    case meal = "meal"
    case hotelStars = "stars"
}

struct Endpoint {
    let path: String
    let queryItems: [URLQueryItem]
}

extension Endpoint {
    var url: URL? {
        var components = URLComponents()
        components.scheme = "https"
        components.host = "tourvisor.ru"
        components.path = path
        components.queryItems = [
            URLQueryItem(name: "authlogin", value: "Kachaev.ka@gmail.com"),
            URLQueryItem(name: "authpass", value: "5tIDN77pKGf0"),
            URLQueryItem(name: "format", value: "json")
        ]
        components.queryItems?.append(contentsOf: queryItems)
        return components.url
    }
}

extension Endpoint {
    static func getLists(typeOf type: Types) -> Endpoint {
        return Endpoint(
            path: "/xml/list.php",
            queryItems: [
                URLQueryItem(name: "type", value: type.rawValue)
            ]
        )
    }
}

class DataFetcherService {
    
    var networkDataFetcher: DataFetcher
    var localDataFetcher: DataFetcher
    
    init(networkDataFetcher: DataFetcher = NetworkDataFetcher(), localDataFetcher: DataFetcher = LocalDataFetcher()) {
        self.networkDataFetcher = networkDataFetcher
        self.localDataFetcher = localDataFetcher
    }
    
    func fetchStars(completion: @escaping (Lists?) -> Void) {
        networkDataFetcher.fetchData(.getLists(typeOf: .hotelStars), response: completion)
    }
    
//    func fetchFreeGames(completion: @escaping (AppGroup?) -> Void) {
//        let urlFreeGames = "https://rss.itunes.apple.com/api/v1/us/ios-apps/top-free-ipad/all/10/explicit.json"
//        networkDataFetcher.fetchData(urlString: urlFreeGames, response: completion)
//
//    }
//
//    func fetchCountry(completion: @escaping ([Country]?) -> Void) {
//        let urlString = "https://raw.githubusercontent.com/Softex-Group/task-mobile/master/test.json"
//        networkDataFetcher.fetchData(urlString: urlString, response: completion)
//
//    }
//
//    func fetchLocalCountry(completion: @escaping ([Country]?) -> Void) {
//        let localUrlString = "usersAPI.txt"
//        localDataFetcher.fetchData(urlString: localUrlString, response: completion)
//    }
}
