//
//  NetworkService.swift
//  NetworkManager
//
//  Created by Кустов Михаил on 12/9/19.
//  Copyright © 2019 Кустов Михаил. All rights reserved.
//

import Foundation

protocol Networking {
    func request(_ endpoint: Endpoint, completion: @escaping (Data?, Error?) -> Void)
}

class NetworkService: Networking {
    
    func request(_ endpoint: Endpoint, completion: @escaping (Data?, Error?) -> Void) {
        guard let url = endpoint.url else { return }
        let request = URLRequest(url: url)
        let task = createDataTask(from: request, completion: completion)
        task.resume()
    }
    
    private func createDataTask(from requst: URLRequest, completion: @escaping (Data?, Error?) -> Void) -> URLSessionDataTask {
        return URLSession.shared.dataTask(with: requst, completionHandler: { (data, response, error) in
            DispatchQueue.main.async {
                completion(data, error)
            }
        })
    }
}
