//
//  FlyOperator.swift
//  NetworkManager
//
//  Created by Кустов Михаил on 12/19/19.
//  Copyright © 2019 Кустов Михаил. All rights reserved.
//

import Foundation

struct FlyOperator {
    let id: Int
    let name: String
    let fullName: String
    let russian: String
    
    init(id: Int, name: String, fullName: String, russian: String) {
        self.id = id
        self.name = name
        self.fullName = fullName
        self.russian = russian
    }
}

extension FlyOperator: Decodable {
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case fullName = "fullname"
        case russian
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let idString = try container.decode(String.self, forKey: .id)
        id = Int(idString)!
        name = try container.decode(String.self, forKey: .name)
        fullName = try container.decode(String.self, forKey: .fullName)
        russian = try container.decode(String.self, forKey: .russian)
    }
}
