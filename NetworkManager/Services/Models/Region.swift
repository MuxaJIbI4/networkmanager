//
//  Region.swift
//  NetworkManager
//
//  Created by Кустов Михаил on 12/19/19.
//  Copyright © 2019 Кустов Михаил. All rights reserved.
//

import Foundation

struct Region {
    let id: Int
    let name: String
    let country: Int
    var selected: Bool = false
    
    init(id: Int, name: String, country: Int, selected: Bool) {
        self.id = id
        self.name = name
        self.country = country
        self.selected = selected
    }
}

extension Region: Decodable {
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case country
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let idString = try container.decode(String.self, forKey: .id)
        id = Int(idString)!
        name = try container.decode(String.self, forKey: .name)
        let countryIdString = try container.decode(String.self, forKey: .country)
        country = Int(countryIdString)!
    }
}
