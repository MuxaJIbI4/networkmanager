//
//  Star.swift
//  NetworkManager
//
//  Created by Кустов Михаил on 12/19/19.
//  Copyright © 2019 Кустов Михаил. All rights reserved.
//

import Foundation

struct Star {
    let id: Int
    let name: String
    
    init(id: Int, name: String) {
        self.id = id
        self.name = name
    }
}

extension Star: Decodable {
    enum CodingKeys: String, CodingKey {
        case id
        case name
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let idString = try container.decode(String.self, forKey: .id)
        id = Int(idString)!
        name = try container.decode(String.self, forKey: .name)
    }
}
