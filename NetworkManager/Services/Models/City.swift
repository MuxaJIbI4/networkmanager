//
//  City.swift
//  NetworkManager
//
//  Created by Кустов Михаил on 12/19/19.
//  Copyright © 2019 Кустов Михаил. All rights reserved.
//

import Foundation

struct City {
    let id: Int
    let name: String
    let nameFrom: String
    var selected: Bool = false
    
    init(id: Int, name: String, nameFrom: String, selected: Bool) {
        self.id = id
        self.name = name
        self.nameFrom = nameFrom
        self.selected = selected
    }
}

extension City: Decodable {
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case nameFrom = "namefrom"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let idString = try container.decode(String.self, forKey: .id)
        id = Int(idString)!
        name = try container.decode(String.self, forKey: .name)
        nameFrom = try container.decode(String.self, forKey: .nameFrom)
    }
}
