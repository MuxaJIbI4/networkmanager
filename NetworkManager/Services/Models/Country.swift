//
//  Country.swift
//  NetworkManager
//
//  Created by Кустов Михаил on 12/19/19.
//  Copyright © 2019 Кустов Михаил. All rights reserved.
//

import Foundation

struct Country {
    let id: Int
    let name: String
    var selected: Bool = false
    
    init(id: Int, name: String, selected: Bool) {
        self.id = id
        self.name = name
        self.selected = selected
    }
}

extension Country: Decodable {
    enum CodingKeys: String, CodingKey {
        case id
        case name
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let idString = try container.decode(String.self, forKey: .id)
        id = Int(idString)!
        name = try container.decode(String.self, forKey: .name)
    }
}
