//
//  Lists.swift
//  NetworkManager
//
//  Created by Кустов Михаил on 12/19/19.
//  Copyright © 2019 Кустов Михаил. All rights reserved.
//

import Foundation

struct Lists {
    let departures: Departures?
    let countries: Destination?
    let regions: Regions?
    let operators: Operators?
    let meals: Meals?
    let stars: Stars?
    let flyDates: FlyDates?
}

extension Lists: Decodable {
    enum CodingKeys: String, CodingKey {
        case lists
    }
    
    enum ListsKeys: String, CodingKey {
        case departures
        case countries
        case regions
        case operators
        case meals
        case stars
        case flyDates = "flydates"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let listsContainer = try container.nestedContainer(keyedBy: ListsKeys.self, forKey: .lists)
        self.departures = try listsContainer.decodeIfPresent(Departures.self, forKey: .departures)
        self.countries = try listsContainer.decodeIfPresent(Destination.self, forKey: .countries)
        self.regions = try listsContainer.decodeIfPresent(Regions.self, forKey: .regions)
        self.operators = try listsContainer.decodeIfPresent(Operators.self, forKey: .operators)
        self.meals = try listsContainer.decodeIfPresent(Meals.self, forKey: .meals)
        self.stars = try listsContainer.decodeIfPresent(Stars.self, forKey: .stars)
        self.flyDates = try listsContainer.decodeIfPresent(FlyDates.self, forKey: .flyDates)
    }
}

struct Departures {
    var departure: [City]
}

extension Departures: Decodable {
    enum CodingKeys: String, CodingKey {
        case departure
    }
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.departure = try container.decodeIfPresent([City].self, forKey: .departure)!
    }
}

struct Destination {
    var country: [Country]
}

extension Destination: Decodable {
    enum CodingKeys: String, CodingKey {
        case country
    }
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.country = try container.decodeIfPresent([Country].self, forKey: .country)!
    }
}

struct Regions {
    var region: [Region]
}

extension Regions: Decodable {
    enum CodingKeys: String, CodingKey {
        case region
    }
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.region = try container.decodeIfPresent([Region].self, forKey: .region)!
    }
}

struct Operators {
    var flyOperator: [FlyOperator]
}

extension Operators: Decodable {
    enum CodingKeys: String, CodingKey {
        case flyOperator = "operator"
    }
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.flyOperator = try container.decodeIfPresent([FlyOperator].self, forKey: .flyOperator)!
    }
}

struct Meals {
    var meal: [Meal]
}

extension Meals: Decodable {
    enum CodingKeys: String, CodingKey {
        case meal
    }
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.meal = try container.decodeIfPresent([Meal].self, forKey: .meal)!
    }
}

struct Stars {
    var star: [Star]
}

extension Stars: Decodable {
    enum CodingKeys: String, CodingKey {
        case star
    }
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.star = try container.decodeIfPresent([Star].self, forKey: .star)!
    }
}

struct FlyDates {
    var flyDate: Array<String>
}

extension FlyDates: Decodable {
    enum CodingKeys: String, CodingKey {
        case flyDate = "flydate"
    }
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.flyDate = try container.decodeIfPresent(Array.self, forKey: .flyDate)!
    }
}


